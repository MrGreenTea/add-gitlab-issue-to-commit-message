import sys
import os
import pathlib

from git import Repo as Repository


def main():
    try:
        _, *branches_to_ignore, msg_file = sys.argv
    except ValueError:
        print(f"Could not get commit message filename because of missing argument: {sys.argv}")
        sys.exit(1)
    if os.environ.get("GIT_EDITOR") != ":":
        # return
        pass
    branch = Repository('.').active_branch.name
    if branch in branches_to_ignore:
        print(f"Ignored branch {branch}.")
        sys.exit(0)
    try:
        issue, *_ = branch.split("-")
        issue = int(issue)
    except ValueError:
        print("Could not read issue number from branch:", branch)
        sys.exit(1)

    msg_file = pathlib.Path(msg_file)
    current_msg = msg_file.read_text()
    msg_file.write_text(f'[#{issue}] {current_msg}')


if __name__ == "__main__":
    main()
